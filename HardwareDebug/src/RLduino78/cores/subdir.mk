################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
..\src/RLduino78/cores/HardwareSerial.cpp \
..\src/RLduino78/cores/IPAddress.cpp \
..\src/RLduino78/cores/Print.cpp \
..\src/RLduino78/cores/RLduino78_RTC.cpp \
..\src/RLduino78/cores/RLduino78_basic.cpp \
..\src/RLduino78/cores/RLduino78_main.cpp \
..\src/RLduino78/cores/Stream.cpp \
..\src/RLduino78/cores/WString.cpp 

C_SRCS += \
..\src/RLduino78/cores/RLduino78_timer.c 

C_DEPS += \
./src/RLduino78/cores/RLduino78_timer.d 

OBJS += \
./src/RLduino78/cores/HardwareSerial.o \
./src/RLduino78/cores/IPAddress.o \
./src/RLduino78/cores/Print.o \
./src/RLduino78/cores/RLduino78_RTC.o \
./src/RLduino78/cores/RLduino78_basic.o \
./src/RLduino78/cores/RLduino78_main.o \
./src/RLduino78/cores/RLduino78_timer.o \
./src/RLduino78/cores/Stream.o \
./src/RLduino78/cores/WString.o 

CPP_DEPS += \
./src/RLduino78/cores/HardwareSerial.d \
./src/RLduino78/cores/IPAddress.d \
./src/RLduino78/cores/Print.d \
./src/RLduino78/cores/RLduino78_RTC.d \
./src/RLduino78/cores/RLduino78_basic.d \
./src/RLduino78/cores/RLduino78_main.d \
./src/RLduino78/cores/Stream.d \
./src/RLduino78/cores/WString.d 


# Each subdirectory must supply rules for building sources it contributes
src/RLduino78/cores/%.o: ../src/RLduino78/cores/%.cpp
	@echo 'Scanning and building file: $<'
	@echo 'Invoking: Scanner and Compiler'
	@rl78-elf-gcc -MM -MP -MF ""$(@:%.o=%.d)"" -MT"$(@:%.o=%.o)" -MT""$(@:%.o=%.d)"" @"src/RLduino78/cores/cpp.depsub" "$<"
	@echo	rl78-elf-gcc -MM -MP -MF "$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -x c++  -fsigned-char -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\include" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\PicalicoFree" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/lib/gcc/rl78-elf/4.8-GNURL78_v14.03/include" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/rl78-elf/include" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\cores" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\cores\avr" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\AndroidAccessory" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\EEPROM" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\EEPROM\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Ethernet" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Ethernet\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Firmata" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\LiquidCrystal" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\RTC" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\SD" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\SD\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Servo" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\SPI" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Stepper" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\USB_Host_Shield" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Wire" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Wire\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\portable\e2studio\RL78" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/lib/gcc/rl78-elf/4.8-GNURL78_v14.03/include" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/rl78-elf/include" -DREL_GR_KURUMI -DARDUINO=100 -DWORKAROUND_READ_MODIFY_WRITE -Os -g2 -g -fno-cprop-registers -fno-rtti "$<"
	@rl78-elf-gcc  @"src/RLduino78/cores/cpp.sub" -o "$(@:%.d=%.o)" "$<"
	@echo rl78-elf-gcc -c -x c++  -fsigned-char -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\include" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\PicalicoFree" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/lib/gcc/rl78-elf/4.8-GNURL78_v14.03/include" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/rl78-elf/include" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\cores" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\cores\avr" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\AndroidAccessory" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\EEPROM" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\EEPROM\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Ethernet" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Ethernet\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Firmata" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\LiquidCrystal" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\RTC" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\SD" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\SD\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Servo" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\SPI" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Stepper" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\USB_Host_Shield" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Wire" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Wire\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\portable\e2studio\RL78" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/lib/gcc/rl78-elf/4.8-GNURL78_v14.03/include" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/rl78-elf/include" -DREL_GR_KURUMI -DARDUINO=100 -DWORKAROUND_READ_MODIFY_WRITE -Os -g2 -g -fno-cprop-registers -fno-rtti -o "$(@:%.d=%.o)" "$<"
	@echo 'Finished scanning and building: $<'
	@echo.

src/RLduino78/cores/%.o: ../src/RLduino78/cores/%.c
	@echo 'Scanning and building file: $<'
	@echo 'Invoking: Scanner and Compiler'
	@rl78-elf-gcc -MM -MP -MF ""$(@:%.o=%.d)"" -MT"$(@:%.o=%.o)" -MT""$(@:%.o=%.d)"" @"src/RLduino78/cores/c.depsub" "$<"
	@echo	rl78-elf-gcc -MM -MP -MF "$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -x c  -fsigned-char -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\include" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\PicalicoFree" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/lib/gcc/rl78-elf/4.8-GNURL78_v14.03/include" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/rl78-elf/include" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\cores" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\cores\avr" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\AndroidAccessory" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\EEPROM" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\EEPROM\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Ethernet" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Ethernet\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Firmata" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\LiquidCrystal" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\RTC" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\SD" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\SD\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Servo" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\SPI" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Stepper" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\USB_Host_Shield" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Wire" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Wire\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\portable\e2studio\RL78" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/lib/gcc/rl78-elf/4.8-GNURL78_v14.03/include" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/rl78-elf/include" -DREL_GR_KURUMI -DARDUINO=100 -DWORKAROUND_READ_MODIFY_WRITE -Os -g2 -g -fno-cprop-registers -fno-rtti "$<"
	@rl78-elf-gcc  @"src/RLduino78/cores/c.sub" -o "$(@:%.d=%.o)" "$<"
	@echo rl78-elf-gcc -c -x c  -fsigned-char -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\include" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\PicalicoFree" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/lib/gcc/rl78-elf/4.8-GNURL78_v14.03/include" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/rl78-elf/include" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\cores" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\cores\avr" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\AndroidAccessory" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\EEPROM" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\EEPROM\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Ethernet" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Ethernet\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Firmata" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\LiquidCrystal" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\RTC" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\SD" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\SD\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Servo" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\SPI" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Stepper" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\USB_Host_Shield" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Wire" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\libraries\Wire\utility" -I"C:\Users\a5034000\Documents\E2Studio\workspace_v4\kurumi_sketch\src\RLduino78\portable\e2studio\RL78" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/lib/gcc/rl78-elf/4.8-GNURL78_v14.03/include" -I"C:\PROGRA~1\KPIT\GNURL7~1.03-\rl78-elf/rl78-elf/include" -DREL_GR_KURUMI -DARDUINO=100 -DWORKAROUND_READ_MODIFY_WRITE -Os -g2 -g -fno-cprop-registers -fno-rtti -o "$(@:%.d=%.o)" "$<"
	@echo 'Finished scanning and building: $<'
	@echo.

